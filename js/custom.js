/*	====================================================
    		FIX HEADER AFFIX JQUERY
  	====================================================
*/
jQuery(window).scroll(function() {
  if (jQuery(this).scrollTop() > 400) {
    jQuery(".navbar").addClass('affix');
  } else {
    jQuery(".navbar").removeClass('affix');
  }
});



/*	====================================================
    		FULLSCREEN MODAL POPUP
  	====================================================
*/
	jQuery(document).ready(function(){
  jQuery(".Modal").hide();
    jQuery(".Ghost-Blue").click(function(){
      jQuery(".Modal").fadeToggle();
    });
    jQuery(".Close").click(function(){
      jQuery(".Modal").fadeOut();
    });
});
// Esc Key, hide menu.
jQuery(document).keydown(function(e) {
if(e.keyCode == 27) {
    jQuery(".Modal").hide(300);
}
});




/*
	====================================================
    		CENTER LOGO ANIMATION
    ====================================================
*/

var logocontent = document.getElementById("logocontent");
            var windowHeight = window.innerHeight;
            var windowWidth = window.innerWidth;
            var scrollArea = window.windowHeight;
            var logotriangle = document.getElementsByClassName("square")[0];
            var logocircle = document.getElementsByClassName("square")[1];
            var logosquare = document.getElementsByClassName("square")[2];
            var logomain = document.getElementsByClassName("square")[3];


            // update position of square 1 and square 2 when scroll event fires.
            window.addEventListener('scroll', function() {
                var scrollTop = window.pageYOffset;
                var scrollPercent = scrollTop/scrollArea || 0;

                //logotriangle.style.left = scrollPercent*window.innerWidth + 'px';
                logotriangle.style.left = scrollPercent*window.innerWidth + '0px';
                logocircle.style.top =  scrollPercent*window.innerWidth + '0px';
                //logocircle.style.right =  -scrollPercent*window.innerWidth + 'px';
                logosquare.style.right =  scrollPercent*window.innerWidth + '0px';

            });
            //var alertbox = document.getElementsByClassName('alertboxt')[0];
            window.onscroll = function () {
                var scrollTop = window.pageYOffset;
                var scrollPercent = scrollTop/scrollArea || 0;
                //show.innerHTML = document.documentElement.scrollTop || document.body.scrollTop;
                //alertbox.value = scrollPercent*window.innerWidth;
                if(scrollPercent*window.innerWidth <= 137.8){
                    logomain.style.bottom =  scrollPercent*window.innerWidth + '0px';
                    logomain.style.right =  scrollPercent*window.innerWidth + '0px';
                    document.getElementsByClassName('orilogo')[0].style.display = 'block';
                    document.getElementsByClassName('blacklogo1')[0].style.display = 'none';
                    document.getElementsByClassName('blacklogo')[0].style.display = 'inline';
                } else {
                    //logomain.style.bottom =  '0px';
                    logomain.style.right =   '416px';
                    document.getElementsByClassName('orilogo')[0].style.display = 'none';
                    document.getElementsByClassName('blacklogo1')[0].style.display = 'block';
                    document.getElementsByClassName('blacklogo')[0].style.display = 'none';
                }
            };



